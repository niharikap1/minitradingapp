package com.test.minitrading.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.test.minitrading.Model.Orders;

@Component
public interface OrderSearchService {

    public Optional<Orders> searchByOrderID(Optional<String> orderID);

    public ResponseEntity searchByUserID(Optional<String> userID);

    public List<Orders> uploadCSVFile(String filePaths);

    List<Orders> uploadCSVFile(MultipartFile file);

}
