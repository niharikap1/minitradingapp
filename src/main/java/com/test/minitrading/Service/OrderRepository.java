package com.test.minitrading.Service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.minitrading.Model.Orders;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Orders, Integer> {

    List<Orders> findByOrderID(String orderID);

    List<Orders> findByUserID(String userID);

    List<Orders> findByOrderType(String orderType);

    List<Orders> findByOrderIDAndOrderType(String orderID, String orderType);

}
