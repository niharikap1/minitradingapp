package com.test.minitrading.Controller;

import com.test.minitrading.ServiceImpl.OrderServiceImpl;
import com.test.minitrading.Model.Orders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;

@RestController
public class MiniTradingController {

    private final Logger logger = LoggerFactory.getLogger(MiniTradingController.class);

    @Autowired
    OrderServiceImpl orderServiceImpl;

    @PostMapping("/upload-csv-file")
    public ResponseEntity<?> bulkUploadInputFiles(@RequestParam("file") MultipartFile file) throws Exception {
        String message = "";
        try {
            orderServiceImpl.uploadOrders(file);
            message = "Uploaded Successfully " + file.getOriginalFilename();
        } catch (Exception ex) {
            logger.info("error occurred while uploading the document");
            throw new Exception("ERROR OCCURRED WHILE UPLOADING THE DOCUMENT" + ex.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(message);
    }


    @GetMapping("/orders/search")
    public ResponseEntity<?> getOrderDetails(@RequestParam HashMap<String, String> search) throws Exception {
        List<Orders> matchedOrder = null;
        String orderID = search.get("orderID");
        String userID = search.get("userID");
        String orderType = search.get("orderType");

        try {
            if (userID != null) {
                matchedOrder = orderServiceImpl.getOrderByUserId(userID);
                logger.info("orders matching the given search criteria fpr userid", matchedOrder);
            } else if (orderID != null) {
                matchedOrder = orderServiceImpl.getOrdersByOrderId(orderID);
                logger.info("orders matching the given search criteria for orderid", matchedOrder);
            } else if(orderType != null){
                matchedOrder = orderServiceImpl.getOrdersByType(orderType);
                logger.info("orders matching the given search criteria for orderType", matchedOrder);
            }
            else {
                matchedOrder = orderServiceImpl.getListOFOrdersByOrderTypeAndOrderID(orderID, orderType);
                logger.info("orders matching the given search criteria for orderid=%s orderType=%s ", orderID, orderType);
            }
        } catch (Exception ex) {
            throw new Exception("EXCEPTION OCCURRED WHILE EXECUTING SEARCH " + ex.getMessage());
        }
        if (matchedOrder.isEmpty()) {
            return new ResponseEntity<>("No Matching Records Found With the Search Criteria", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(matchedOrder, HttpStatus.OK);

    }
}


