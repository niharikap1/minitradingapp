package com.test.minitrading.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import com.test.minitrading.Model.Orders;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;


public class ReadCSVUtility {

    public static List<Orders> parseCSVFile(InputStream inputStream) throws IOException, CsvException {
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

        CSVReader reader = new CSVReaderBuilder(br).
                withSkipLines(1).
                build();
        List<Orders> orderList = reader.readAll().stream().map(data -> {
            Orders orderObj = new Orders();
            orderObj.setOrderID(data[0]);
            orderObj.setUserID(data[1]);
            orderObj.setCurrencyPair(data[2]);
            orderObj.setOrderType(data[3]);
            orderObj.setAmount(Integer.parseInt(data[4]));
            orderObj.setDate(data[5]);
            orderObj.setDirections(data[6]);
            orderObj.setStatus(data[7]);

            return orderObj;
        }).collect(Collectors.toList());

        return orderList;

    }
}
	
	


