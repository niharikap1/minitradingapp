package com.test.minitrading.ServiceImpl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.test.minitrading.Model.Orders;
import com.test.minitrading.Service.OrderRepository;
import com.test.minitrading.Utility.ReadCSVUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.exceptions.CsvException;

import javax.annotation.PostConstruct;

@Service
public class OrderService {
//    {
//        FileInputStream fileInputStream = null;
//        try {
//            fileInputStream = new FileInputStream("/src/main/resources/OrdersUpdated.csv");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        try {
//            ReadCSVUtility.parseCSVFile(fileInputStream);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (CsvException e) {
//            e.printStackTrace();
//        }
//    }

    @PostConstruct
    public void start(){
        //Initialize the orders
       // initializeOrders();

    }

    @Autowired
    OrderRepository orderRepo;

    public void save(MultipartFile file) {
        List<Orders> orderList = null;
        try {
            orderList = ReadCSVUtility.parseCSVFile(file.getInputStream());
        } catch (IOException | CsvException e) {
            e.printStackTrace();
        }
        orderRepo.saveAll(orderList);
    }


//    public Optional<Orders> getListOfOrderByID(String id) {
//        return orderRepo.findById(Integer.parseInt(id));
//    }
//
    public Optional<Orders> getListOfOrderByUserID(String id) {
        return orderRepo.findById(Integer.valueOf(id));
    }

    public List<Orders> getAllOrders() {
        return orderRepo.findAll();
    }
}
