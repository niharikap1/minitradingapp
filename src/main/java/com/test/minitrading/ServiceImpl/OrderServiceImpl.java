package com.test.minitrading.ServiceImpl;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.test.minitrading.Controller.MiniTradingController;
import com.test.minitrading.Utility.ReadCSVUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.test.minitrading.Model.Orders;
import com.test.minitrading.Service.OrderRepository;
import com.opencsv.exceptions.CsvException;

@Service
public class OrderServiceImpl {

    private final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    OrderRepository orderRepository;

    public void uploadOrders(MultipartFile file) {
        List<Orders> orderList = null;
        try {
            orderList = ReadCSVUtility.parseCSVFile(file.getInputStream());
            if(!orderList.isEmpty()){
                orderRepository.saveAll(orderList);
            }
        } catch (IOException | CsvException e) {
            logger.error("something went wrong while uploading orders", e.getMessage());
        }

    }

    /**
     * filters orders by userid
     * @param orderId
     * @return
     */
    public List<Orders> getOrdersByOrderId(String orderId){
        List<Orders> orderList = orderRepository.findByOrderID(orderId);
        logger.info("get orders by order id "+ orderList);
        return sortList(orderList, "orderID");
    }

    public List<Orders> getOrderByUserId(String userId){
        List<Orders> orderReceived = orderRepository.findByUserID(userId);
        logger.info("get orders by userid id %s "+ orderReceived.size());
        return sortList(orderReceived, "userId");
    }

    public List<Orders> getListOFOrdersByOrderTypeAndOrderID(String id, String type){
        List<Orders> orderByType = orderRepository.findByOrderIDAndOrderType(id, type);
        logger.info("Order By OrderType "+orderByType);
        return orderByType;
    }

    public List<Orders> getOrdersByType(String orderType){
        List<Orders> orderByType = orderRepository.findByOrderType(orderType);
        logger.info("Order By OrderType "+orderByType);
        return orderByType;
    }


    /**
     *  Sorting the orders by  orderId
      * @param  orderByUserId
     * @param sortType
     * @return
     */
    private List<Orders> sortList(List<Orders> orderByUserId, String sortType) {
        List<Orders> sortedOrderList = orderByUserId.stream().sorted(Comparator.comparing(Orders ::getOrderID)).collect(Collectors.toList());
        return sortedOrderList;
    }

}

